export const MenuConfigs = [{
    id: "trending",
    menuname: 'Trending',
    class: 'color-1',
    subclass: 'fa fa-play-circle-o'
}, {
    id: "sports",
    menuname: "Sports",
    class: 'color-2',
    subclass: 'fa fa-soccer-ball-o'
}, {
    id: "entertainment",
    menuname: "Entertainment",
    class: 'color-3',
    subclass: 'fa fa-music'
}, {
    id: "dailynews",
    menuname: "News",
    class: 'color-4',
    subclass: 'fa fa-globe'
}, {
    id: "lifestyle",
    menuname: "Lifestyle",
    class: 'color-5',
    subclass: 'fa fa-gamepad'
}, {
    id: "infotainment",
    menuname: "Infotainment",
    class: 'color-6',
    subclass: 'fa fa-th-large'
}]