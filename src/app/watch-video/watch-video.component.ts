import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SocialdataService } from "../core-service/socialdata.service";
@Component({
  selector: 'app-watch-video',
  templateUrl: './watch-video.component.html',
  styleUrls: ['./watch-video.component.scss']
})
export class WatchVideoComponent implements OnInit {
  public id;
  public singleVideo;
  constructor(
    private route: ActivatedRoute,
    private socialapi: SocialdataService,
  ) {
    this.route.data.subscribe(params => {
      console.log("param", params);
      this.id = params['id'];
      console.log("data", this.id);
    })
  }

  ngOnInit() {
    this.onWatchVideo(this.id);
  }
  onWatchVideo(id) {
    this.socialapi.getSingleVideo(id)
      .subscribe(
      res => console.log("singl", res))
  }

}
