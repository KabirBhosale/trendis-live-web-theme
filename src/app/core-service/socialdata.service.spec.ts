import { TestBed, inject } from '@angular/core/testing';

import { SocialdataService } from './socialdata.service';

describe('SocialdataService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SocialdataService]
    });
  });

  it('should be created', inject([SocialdataService], (service: SocialdataService) => {
    expect(service).toBeTruthy();
  }));
});
