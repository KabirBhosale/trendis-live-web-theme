import { Injectable } from '@angular/core';
import { Http } from "@angular/http";
import "rxjs";
import 'rxjs/add/operator/catch';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class SocialdataService {

  private usersUrl: string = 'http://www.trendis.com/api/';

  constructor(
    private http: Http,
  ) { }


  /**
   * Getting Social Data from Backend with
   * sending pagenumber with scroll event
   * @param 
   */
  getSocialData(pagenumber) {
    // console.log("service page number", pagenumber);
    return this.http
      .get(this.usersUrl + `feeds/top?country=IN&media=photo,video,link&category=entertainment&subcategory=all&page=${pagenumber}`)
      .map(res => res.json()).catch((error: any) => {
        if (error.status == 404 || error.status == 500) {
          return Observable.throw(new Error(error.status));
        }
      });
  }

  getSingleVideo(id) {
    return this.http
    .get(this.usersUrl + id).map(res => res.json());
  }

}
