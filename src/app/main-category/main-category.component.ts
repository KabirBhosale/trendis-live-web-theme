import { Component, OnInit } from '@angular/core';
import {MenuConfigs} from "../../assets/config/menuconfig";

@Component({
  selector: 'app-main-category',
  templateUrl: './main-category.component.html',
  styleUrls: ['./main-category.component.scss']
})
export class MainCategoryComponent implements OnInit {
  menuConfigs: any[];
  constructor() {
    this.menuConfigs = MenuConfigs;
  }

  ngOnInit() {
  }

}
