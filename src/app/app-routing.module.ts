import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainSectionComponent } from "./main-section/main-section.component";
import { VideoSectionComponent } from "./video-section/video-section.component";
import { WatchVideoComponent } from './watch-video/watch-video.component';
const routes: Routes = [
  {
    path: '',
    redirectTo: 'livevideo',
    pathMatch: 'full'
  },
  {
    path: 'livevideo',
    component: VideoSectionComponent
  },
  {
    path: 'watch/:id',
    component: WatchVideoComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
