import { Component, OnInit, HostListener } from '@angular/core';
import { SocialdataService } from "../core-service/socialdata.service";
import { ToastsManager, ToastOptions } from 'ng2-toastr/ng2-toastr';
@Component({
  selector: 'app-video-section',
  templateUrl: './video-section.component.html',
  styleUrls: ['./video-section.component.scss'],
  providers: [ToastsManager, ToastOptions]
})
export class VideoSectionComponent implements OnInit {

  public pagenumber: number = 1;
  public socialusers;

  constructor(
    private socialapi: SocialdataService,
    public toastr: ToastsManager,
  ) { }

  @HostListener("window:scroll", [])
  onWindowScorll(): void {
    if ((window.innerHeight + window.scrollY) >= document.body.offsetHeight) {
      this.onScroll();
    }
  }
  ngOnInit() {
    this.socialData();
  }


  socialData() {
    this.socialapi.getSocialData(this.pagenumber++)
      .subscribe(res => {
        this.socialusers = res;
        console.log(res);
      })
  }

  /**
   * when user scroll call to backend service.
   */
  onScroll() {
    let appendCards;
    if (this.pagenumber >= 50)
      return false;
    this.socialapi.getSocialData(this.pagenumber++)
      .subscribe(res => {
        res.forEach(function (el) {
          appendCards = Object.assign({}, el);
        });
        this.socialusers.push(appendCards);
      })
  }

  /**
   * Displaying the toaster when the data load completely.
   */
  showInfo() {
    this.toastr.info('Just some information for you.');
  }

}
