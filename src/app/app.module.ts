import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { HttpModule } from "@angular/http";
import { AppComponent } from './app.component';
import "rxjs/add/operator/map";
import { HeaderBarComponent } from './header-bar/header-bar.component';
import { MainCategoryComponent } from './main-category/main-category.component';
import { MainSectionComponent } from './main-section/main-section.component';
import { SideNavSectionComponent } from './side-nav-section/side-nav-section.component';
import { VideoSectionComponent } from './video-section/video-section.component';
import { SocialdataService } from "./core-service/socialdata.service"
import { ToastModule } from 'ng2-toastr/ng2-toastr';
import { WatchVideoComponent } from './watch-video/watch-video.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderBarComponent,
    MainCategoryComponent,
    MainSectionComponent,
    SideNavSectionComponent,
    VideoSectionComponent,
    WatchVideoComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    AppRoutingModule,
    ToastModule

  ],
  providers: [SocialdataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
export const routedComponents = [
  HeaderBarComponent,
  MainCategoryComponent,
  MainSectionComponent,
  SideNavSectionComponent,
  VideoSectionComponent,
  WatchVideoComponent
];
